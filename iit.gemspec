Gem::Specification.new do |s|
  s.name        = 'iit'
  s.version     = '0.0.1'
  s.date        = '2014-12-02'
  s.summary     = "IBM i Toolkit for Ruby"
  s.description = "A collection of APIs used to more easily interact with IBM i from the Ruby language."
  s.authors     = ["Aaron Bartell"]
  s.email       = 'team@litmis.com'
  s.files       = ["lib/iit.rb"]
  s.homepage    = 'https://bitbucket.org/litmis/iit'
  s.license       = 'MIT'
end