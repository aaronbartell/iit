require 'iit'

describe IIT do
  describe "#qcmdexc" do
    it "successfully invokes command" do
      actual = IIT.qcmdexc "DSPSYSVAL QMODEL"
      expect(actual).to match(/E N D  O F  L I S T I N G/)
    end
  end
  describe "#run_sql" do
    it "successfully runs statement" do
      actual = IIT.run_sql "SELECT * FROM QIWS.QCUSTCDT"
      expect(actual).to match(/CUSNUM/)
    end
  end
  describe "#which" do
    it "locates binary" do
      actual = IIT.which "ruby"
      expect(actual).to match(/ruby/)
    end
  end
  describe "#user_exists?" do
    it "checks user existence" do
      actual = IIT.user_exists? "QSECOFR"
      expect(actual).to eq(true)
    end
  end
end