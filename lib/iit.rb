module IIT
  extend self
  
  def qcmdexc cmd
    output = `/QOpenSys/usr/bin/system \"#{cmd}\" 2>&1`
  end
  
  def run_sql cmd
    qcmdexc "CALL PGM(QZDFMDB2) PARM('#{cmd}')"
  end
  
  def sys cmd
    system cmd
  end
  
  def which(cmd)
    exts = ENV['PATHEXT'] ? ENV['PATHEXT'].split(';') : ['']
    ENV['PATH'].split(File::PATH_SEPARATOR).each do |path|
      exts.each { |ext|
        exe = File.join(path, "#{cmd}#{ext}")
        return exe if File.executable? exe
      }
    end
    return nil
  end
   
  def DSPOBJD(lib, obj, type)
    lib ||= '*LIBL'
    qcmdexc "DSPOBJD OBJ(#{lib}/#{obj}) OBJTYPE(#{type}) DETAIL(*FULL)"
  end

  def DSPFFD(lib, obj)
    lib ||= '*LIBL'
    qcmdexc "DSPFFD FILE(#{lib}/#{obj}) OUTPUT(*)"
  end

  def DSPFD(lib, obj, type)
    lib ||= '*LIBL'
    qcmdexc "DSPFD FILE(#{lib}/#{obj}) TYPE(#{type}) OUTPUT(*)"
  end

  def DSPDTAARA(lib, obj)
    lib ||= '*LIBL'
    result = qcmdexc "DSPDTAARA DTAARA(#{lib}/#{obj}) OUTPUT(*) OUTFMT(*CHAR)"
    type   = /.*TYPE(.*)\n/.match(result)[1].strip rescue ''
    length = /.*LEN(.*)\n/.match(result)[1].strip rescue ''
    text   = /.*TEXT(.*)\n/.match(result)[1].strip rescue ''
    value  = /.*VALUE(.*)\n/.match(result)[1].strip rescue ''
    {type: type, length: length, text: text, value: value}  #returning a hash for simple access: DSPDTAARA(...)[:type]
  end
  
  def DSPSYSVAL(value)
    result = qcmdexc "DSPSYSVAL SYSVAL(#{value})"
    /QMODEL\s+([0-9A-Z]+)/.match(result)[1].strip rescue ''
  end
  
  def object_created_by(lib, obj, type)
    result = DSPOBJD(lib, obj, type)
    /.*Created by user  . . . . . . . . . . :(.*)\n/.match(result)[1].strip
  end
  
  def object_attribute(lib, obj, type)
    result = DSPOBJD(lib, obj, type)
    /.*Attribute  . . . . . :(.*)\n/.match(result)[1].strip
  end  

  def srcpf?(lib, obj)
    DSPFFD(lib.chomp('.LIB'), obj.chomp('.FILE')).scan(/SRCSEQ|SRCDAT|SRCDTA/).length == 3
  end

  def src_type(lib, srcpf, mbr)
    result = DSPFD(lib.chomp('.LIB'), srcpf.chomp('.FILE'), '*MBRLIST')  
    /^(?=.*\s\s#{mbr.chomp('.MBR')}\s).{30}(.{7})/.match(result)[1].strip rescue ''
  end
    
  def lib_exists? lib
    !(qcmdexc "CHKOBJ OBJ(#{lib}) OBJTYPE(*LIB)").include? "CPF9801"
  end

  def obj_exists? lib, obj, type
    !(qcmdexc "CHKOBJ OBJ(#{lib}/#{obj}) OBJTYPE(#{type})").include? "CPF9801"
  end
  
  def user_exists? user_name
    !(qcmdexc "CHKOBJ OBJ(#{user_name}) OBJTYPE(*USRPRF)").include? "CPF9801"
  end
end